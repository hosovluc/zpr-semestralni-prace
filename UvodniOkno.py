from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtSql import *
from HlavniOkno import *
from Spravce import *
from DBhandler import *

class UvodniOkno(QWidget):
    def __init__(self):
        super().__init__()

        # vytvoreni databaze
        db = DBhandler()
        db.createDB()

        # nastaveni okna
        self.setWindowTitle('Kniha jízd')
        self.setGeometry(750, 400, 400, 400)

        # widgety - vytvoreni tlacitek, label
        self.btn1 = QComboBox()
        self.btn3 = QPushButton('Pokračovat')
        self.napis = QLabel('Vyberte uživatele:')

        # Combobox Items
        db = DBhandler()
        q = db.get_all_users()
        while q.next():
            self.btn1.addItem(q.value(0))

        self.btn1.addItem('Správce')

        # nastaveni layoutu
        hlavni_layout = QVBoxLayout()
        rozlozeni_tlacitek = QGridLayout()
        hlavni_layout.addWidget(self.napis)

        hlavni_layout.addLayout(rozlozeni_tlacitek)
        self.setLayout(hlavni_layout)

        # rozmisteni tlacitek
        rozlozeni_tlacitek.addWidget(self.btn1)
        rozlozeni_tlacitek.addWidget(self.btn3)

        # nastaveni lablu
        self.napis.setAlignment(Qt.AlignCenter)
        self.napis.setFont(QFont('Verdana', 25))

        # propojeni tlacitek
        self.btn3.clicked.connect(self.pokracovat)

    def pokracovat(self):
        prihlaseny = self.btn1.currentText()
        if prihlaseny == 'Správce':
            self.spravce = Spravce()
            self.spravce.show()
        else:
            self.hlavni_okno = HlavniOkno(prihlaseny)
            self.hlavni_okno.show()