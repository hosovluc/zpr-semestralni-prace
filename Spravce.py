from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtSql import *
from DBhandler import *
from Table2 import *
from NovyZaznam import *
from UvodniOkno import *
from NovyUzivatel import *

class Spravce(QWidget):
    def __init__(self):
        super().__init__()

        # nastaveni okna
        self.setWindowTitle('Kniha jízd')
        self.setGeometry(750, 400, 400, 400)

        # widgety - vytvoreni tlacitek, labelu
        self.btn1 = QPushButton('Zobraz záznamy všech uživatelů')
        self.btn2 = QPushButton('Přidej nového uživatele')

        self.napis = QLabel('Vítejte v knize jízd!')
        self.napis2 = QLabel('Přihlášený uživatel: Správce')

        # nastaveni rozmisteni (layout)
        hlavni_layout = QVBoxLayout()
        rozlozeni_tlacitek = QVBoxLayout()
        hlavni_layout.addWidget(self.napis2)
        hlavni_layout.addWidget(self.napis)

        hlavni_layout.addLayout(rozlozeni_tlacitek)
        self.setLayout(hlavni_layout)

        # rozmisteni tlacitek
        rozlozeni_tlacitek.addWidget(self.btn1)
        rozlozeni_tlacitek.addWidget(self.btn2)

        # nastaveni lablu
        self.napis.setAlignment(Qt.AlignCenter)
        self.napis.setAlignment(Qt.AlignTop)
        self.napis.setFont(QFont('Verdana', 25))

        self.napis2.setAlignment(Qt.AlignTop)
        self.napis2.setAlignment(Qt.AlignRight)

        # propojeni tlacitek
        self.btn1.clicked.connect(self.zobraz_tabulku)
        self.btn2.clicked.connect(self.novy_uzivatel)

    def zobraz_tabulku(self):
        self.tabulka = Table2()
        self.tabulka.show()

    def novy_uzivatel(self):
        self. nove_okno = NovyUzivatel()
        self. nove_okno.show()