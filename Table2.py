from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtSql import *
from DBhandler import *

class Table2 (QWidget):
    def __init__(self):
        super().__init__()

        # nastaveni okna
        self.setWindowTitle("Záznamy všech uživatelů")
        self.setGeometry(750, 400, 600, 400)

        # nastaveni layoutu
        layout1 = QVBoxLayout()
        self.setLayout(layout1)

        # inicializace modelu a nahledu
        db = DBhandler()
        global model
        model = db.initializeModel2()
        global view1
        view1 = self.createView(model)

        layout1.addWidget(view1)

        # vytvoreni tlacitka
        self.btn_delete = QPushButton('Odeber řádek')
        layout1.addWidget(self.btn_delete)
        self.btn_delete.clicked.connect(self.odeberRadek)

    def createView(self, model):
        view = QTableView()
        view.setModel(model)
        view.setWindowTitle('Záznamy')
        return view

    def odeberRadek(self):
        db = DBhandler()
        db.delete(view1.model().index(view1.currentIndex().row(), 0).data())
        model.removeRow(view1.currentIndex().row())

