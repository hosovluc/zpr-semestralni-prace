from PyQt5.QtSql import QSqlDatabase, QSqlQuery, QSqlTableModel
from PyQt5.QtCore import *

class DBhandler:
    def createDB(self):
        db = QSqlDatabase.addDatabase('QSQLITE')
        db.setDatabaseName('knihajizd.db')
        db.open()
        self.initData()

    def initData(self):
        q = QSqlQuery()
        q.exec("create table tabulka_uzivatelu (id integer primary key autoincrement, uzivatel varchar(20) not null, najete_km int default 0);")
        q.exec("CREATE TABLE seznam (id INTEGER PRIMARY KEY AUTOINCREMENT, uzivatel INTEGER REFERENCES tabulka_uzivatelu (id) ON DELETE CASCADE, odkud VARCHAR (20), kam VARCHAR (20), pocet_km INTEGER);")

    def DBinsert(self, uzivatel, odkud, kam, km):
        q = QSqlQuery()
        q.prepare("INSERT INTO seznam (uzivatel, odkud, kam, pocet_km) VALUES (:uzivatel, :odkud, :kam, :pocet_km)")
        q.bindValue(0, uzivatel)
        q.bindValue(1, odkud)
        q.bindValue(2, kam)
        q.bindValue(3, km)
        q.exec()

    def DBinsert2(self, uzivatel):
        q = QSqlQuery()
        q.prepare("INSERT INTO tabulka_uzivatelu (uzivatel) VALUES (:uzivatel)")
        q.bindValue(0, uzivatel)
        q.exec()

    def initializeModel(self, prihlaseny):
        model = QSqlTableModel()
        model.setTable('seznam')
        a = self.get_user_by_name(prihlaseny)
        b = f'uzivatel = {a}'
        model.setFilter(b)
        model.select()

        model.setHeaderData(0, Qt.Horizontal, "ID")
        model.setHeaderData(1, Qt.Horizontal, "Uživatel")
        model.setHeaderData(2, Qt.Horizontal, "Odkud")
        model.setHeaderData(3, Qt.Horizontal, "Kam")
        model.setHeaderData(4, Qt.Horizontal, "Počet km")
        return model

    def initializeModel2(self):
        model = QSqlTableModel()
        model.setTable('tabulka_uzivatelu')
        model.select()
        model.setHeaderData(0, Qt.Horizontal, "ID")
        model.setHeaderData(1, Qt.Horizontal, "Uživatel")
        model.setHeaderData(2, Qt.Horizontal, "Najeté km")
        return model

    def get_all_users(self):
        q = QSqlQuery()
        q.exec("select uzivatel from tabulka_uzivatelu")
        return q

    def get_user_by_name(self, jmeno):
        q = QSqlQuery()
        q.prepare("select id from tabulka_uzivatelu where uzivatel = :jmeno")
        q.bindValue(0, jmeno)
        q.exec()
        q.first()
        return q.value(0)

    def get_najete_km(self, jmeno):
        q = QSqlQuery()
        q.prepare("select najete_km from tabulka_uzivatelu where uzivatel = :jmeno")
        q.bindValue(0, jmeno)
        q.exec()
        q.first()
        return q.value(0)

    def update_km(self, km, uzivatel):
        q = QSqlQuery()
        q.prepare("UPDATE tabulka_uzivatelu set najete_km = :km where uzivatel = :uzivatel")
        q.bindValue(0, km)
        q.bindValue(1, uzivatel)
        q.exec()

    def delete(self, id):
        q = QSqlQuery()
        q.prepare("DELETE FROM seznam where uzivatel = :id")
        q.bindValue(0, id)
        q.exec()