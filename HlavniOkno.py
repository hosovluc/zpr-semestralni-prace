from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtSql import *
from DBhandler import *
from Table import *
from NovyZaznam import *
from UvodniOkno import *

class HlavniOkno(QWidget):
    prihlaseny = ''
    def __init__(self, prihlaseny):
        super().__init__()
        self.prihlaseny = prihlaseny

        # nastaveni okna
        self.setWindowTitle('Kniha jízd')
        self.setGeometry(750, 400, 400, 400)

        # widgety - vytvoreni tlacitek, labelu
        self.btn1 = QPushButton('Zobrazit záznamy')
        self.btn2 = QPushButton('Přidej nový záznam')

        a = f'Přihlášený uživatel: {self.prihlaseny}'
        self.napis2 = QLabel(a)
        self.napis = QLabel('Vítejte v knize jízd!')

        # nastaveni layoutu
        hlavni_layout = QVBoxLayout()
        rozlozeni_tlacitek = QGridLayout()
        hlavni_layout.addWidget(self.napis2)
        hlavni_layout.addWidget(self.napis)

        hlavni_layout.addLayout(rozlozeni_tlacitek)
        self.setLayout(hlavni_layout)

        # rozmisteni tlacitek
        rozlozeni_tlacitek.addWidget(self.btn1)
        rozlozeni_tlacitek.addWidget(self.btn2)

        # nastaveni lablu
        self.napis.setAlignment(Qt.AlignCenter)
        self.napis.setAlignment(Qt.AlignTop)
        self.napis.setFont(QFont('Verdana', 25))

        self.napis2.setAlignment(Qt.AlignTop)
        self.napis2.setAlignment(Qt.AlignRight)

        # propojeni tlacitek
        self.btn1.clicked.connect(self.zobraz_tabulku)
        self.btn2.clicked.connect(self.novy_zaznam)

    def zobraz_tabulku(self):
        self.tabulka = Table(self.prihlaseny)
        self.tabulka.show()

    def novy_zaznam(self):
        self.nove_okno = NovyZaznam(self.prihlaseny)
        self.nove_okno.show()