from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from DBhandler import *

class NovyUzivatel(QWidget):
    def __init__(self):
        super().__init__()

        # nastaveni okna
        self.setWindowTitle('Nový uživatel')
        self.setGeometry(750, 400, 400, 400)

        # widgety - vytvoreni tlacitek, label
        self.napis1 = QLabel('Přidej nového uživatele')
        self.btn_submit = QPushButton('Potvrdit')

        self.zadavaci_pole = QLineEdit()
        self.zadavaci_pole.setPlaceholderText('Jméno nového uživatele:')

        # nastaveni layoutu
        rozlozeni_form = QGridLayout()
        rozlozeni_form.addWidget(self.napis1)
        self.setLayout(rozlozeni_form)

        # rozmisteni tlacitek
        rozlozeni_form.addWidget(self.zadavaci_pole)
        rozlozeni_form.addWidget(self.btn_submit)

        # nastaveni lablu
        self.napis1.setAlignment(Qt.AlignCenter)
        self.napis1.setFont(QFont('Verdana', 25))

        # propojeni tlacitka
        self.btn_submit.clicked.connect(self.submit)

    def submit(self):
        db = DBhandler()
        db.DBinsert2(self.zadavaci_pole.text())
        self.hide()
