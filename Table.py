from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtSql import *
from DBhandler import *

class Table (QWidget):
    prihlaseny = ''
    def __init__(self, prihlaseny):
        super().__init__()
        self.prihlaseny = prihlaseny

        # nastaveni okna
        napis = f'Záznamy uživatele {self.prihlaseny}'
        self.setWindowTitle(napis)
        self.setGeometry(750, 400, 700, 400)

        # nastaveni layoutu
        layout1 = QVBoxLayout()
        self.setLayout(layout1)

        # inicializace modelu a náhledu
        db = DBhandler()
        global model
        model = db.initializeModel(self.prihlaseny)
        global view1
        view1 = self.createView(model)

        layout1.addWidget(view1)

        # tlacitko Odeber radek
        self.btn_delete = QPushButton('Odeber řádek')
        layout1.addWidget(self.btn_delete)
        self.btn_delete.clicked.connect(self.odeberRadek)

    def createView(self, model):
        view = QTableView()
        view.setModel(model)
        view.setWindowTitle('Zaznamy')
        return view

    def odeberRadek(self):
        self.del_update()
        model.removeRow(view1.currentIndex().row())

    def del_update(self):
        db = DBhandler()
        stavajici = int(db.get_najete_km(self.prihlaseny))
        mazane = view1.model().index(view1.currentIndex().row(), 4).data()
        odecet = stavajici - mazane
        db.update_km(odecet,self.prihlaseny)