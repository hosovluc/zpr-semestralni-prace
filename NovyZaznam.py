from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from DBhandler import *

class NovyZaznam(QWidget):
    prihlaseny = ''
    def __init__(self, prihlaseny):
        super().__init__()
        self.prihlaseny = prihlaseny

        # nastaveni okna
        self.setWindowTitle('Nový záznam')
        self.setGeometry(750, 400, 400, 400)

        # widgety - vytvoreni tlacitek, label
        self.napis1 = QLabel('Zapiš novou cestu')
        self.btn_submit = QPushButton('Potvrdit')

        self.zadavaci_pole = QLineEdit()
        self.zadavaci_pole.setPlaceholderText('Odkud:')

        self.zadavaci_kam = QLineEdit()
        self.zadavaci_kam.setPlaceholderText('Kam:')

        self.zadavaci_km = QLineEdit()
        self.zadavaci_km.setPlaceholderText('Počet kilometrů:')

        # nastaveni layoutu
        rozlozeni_form = QGridLayout()
        rozlozeni_form.addWidget(self.napis1)
        self.setLayout(rozlozeni_form)

        # rozmisteni tlacitek
        rozlozeni_form.addWidget(self.zadavaci_pole)
        rozlozeni_form.addWidget(self.zadavaci_kam)
        rozlozeni_form.addWidget(self.zadavaci_km)
        rozlozeni_form.addWidget(self.btn_submit)

        # nastaveni lablu
        self.napis1.setAlignment(Qt.AlignCenter)
        self.napis1.setFont(QFont('Verdana', 25))

        # propojeni tlacitka
        self.btn_submit.clicked.connect(self.submit)

    def submit(self):
        db = DBhandler()
        a = db.get_user_by_name(self.prihlaseny)
        db.DBinsert(a, self.zadavaci_pole.text(), self.zadavaci_kam.text(), self.zadavaci_km.text())
        self.update()
        self.hide()

    def update(self):
        db = DBhandler()
        stavajici_km = int(db.get_najete_km(self.prihlaseny))
        novy_km = int(self.zadavaci_km.text())
        soucet = stavajici_km + novy_km
        db.update_km(soucet, self.prihlaseny)